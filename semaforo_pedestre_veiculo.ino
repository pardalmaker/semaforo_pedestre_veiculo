//
// Semáforo Pedestre - Veículo
// Vídeo de teste: https://www.instagram.com/p/C0frI1Lo4J5
// 
// Testado no Arduino Uno
// 
// Por Ericson Benjamim @pardalmaker
//

int verdeRua = 2;
int amareloRua = 3;
int vermelhoRua = 4;

int verdePedestre = 5;
int vermelhoPedestre = 6;

int tempo1 = 2000;
int tempo2 = 1000;

int acender = HIGH;
int apagar = LOW;

void setup() {
  // Inicializacao
  pinMode(verdeRua, OUTPUT);
  pinMode(amareloRua, OUTPUT);
  pinMode(vermelhoRua, OUTPUT);
 
  pinMode(verdePedestre, OUTPUT);
  pinMode(vermelhoPedestre, OUTPUT);
}

void loop() {
  // Fecha para Rua
  digitalWrite(vermelhoRua, acender);
  digitalWrite(amareloRua, apagar);
  digitalWrite(verdeRua, apagar);

  // Abre para Pedestre
  digitalWrite(vermelhoPedestre, apagar);

  digitalWrite(verdePedestre, acender);

  delay(tempo1);

  // Fecha para Pedestre
  digitalWrite(vermelhoPedestre, acender);
  digitalWrite(verdePedestre, apagar);

  delay(tempo2);

  // Abre para Rua
  digitalWrite(vermelhoRua, apagar);
  digitalWrite(amareloRua, apagar);
  digitalWrite(verdeRua, acender);

  delay(tempo1);

  // Amarelo para Rua
  digitalWrite(vermelhoRua, apagar);
  digitalWrite(amareloRua, acender);
  digitalWrite(verdeRua, apagar);

  delay(tempo1);
}
